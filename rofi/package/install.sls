# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as rofi with context %}

rofi-package-install-pkgs-installed:
  pkg.installed:
    - pkgs: {{ rofi.pkgs }}
