# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as rofi with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('rofi-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_rofi', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

rofi-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

rofi-config-file-rofi-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/rofi
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - rofi-config-file-user-{{ name }}-present

{% for conffile in ['config', 'colours', 'emoji'] %}
rofi-config-file-rofi-config-{{ conffile }}-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/rofi/{{ conffile }}.rasi
    - source: {{ files_switch([
                  name ~ '-' ~ conffile ~ '.rasi.tmpl',
                  conffile ~ '.rasi.tmpl'],
                lookup='rofi-config-file-rofi-config-' ~ conffile ~ '-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        scaling_factor: {{ salt.grains.get('scaling_factor', 1 ) }}
    - require:
      - rofi-config-file-rofi-dir-{{ name }}-managed
{% endfor %}

{% endif %}
{% endfor %}
{% endif %}
