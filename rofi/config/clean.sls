# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as rofi with context %}

{% if salt['pillar.get']('rofi-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_rofi', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

{% for conffile in ['config', 'colours', 'emoji'] %}
rofi-config-clean-rofi-config-{{ conffile }}-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/rofi/{{ conffile }}.rasi
{% endfor %}

rofi-config-clean-rofi-dir-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/rofi

{% endif %}
{% endfor %}
{% endif %}
