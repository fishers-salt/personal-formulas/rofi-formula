# frozen_string_literal: true

packages =
  case os[:name]
  when 'arch'
    %w[rofi rofi-emoji]
  else
    %w[rofi]
  end

packages.each do |pkg|
  control "rofi-package-clean-pkg-#{pkg}-absent" do
    title "#{pkg} should not be installed"

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
