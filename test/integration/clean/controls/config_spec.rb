# frozen_string_literal: true

conffiles = %w[config colours emoji]

conffiles.each do |conffile|
  control "rofi-config-clean-rofi-config-#{conffile}-auser-absent" do
    title 'should be absent'

    describe file("/home/auser/.config/rofi/#{conffile}.rasi") do
      it { should_not exist }
    end
  end
end

control 'rofi-config-clean-rofi-dir-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/rofi') do
    it { should_not exist }
  end
end
