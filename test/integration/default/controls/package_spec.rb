# frozen_string_literal: true

packages =
  case os[:name]
  when 'arch'
    %w[rofi rofi-emoji]
  else
    %w[rofi]
  end

packages.each do |pkg|
  control "rofi-package-install-pkg-#{pkg}-installed" do
    title "#{pkg} should be installed"

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
