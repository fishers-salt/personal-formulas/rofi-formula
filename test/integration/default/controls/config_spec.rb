# frozen_string_literal: true

control 'rofi-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'rofi-config-file-rofi-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/rofi') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

conffiles = {
  config: 'font: "fira code medium 10";',
  colours: '@theme "/usr/share/rofi/themes/gruvbox-dark.rasi"',
  emoji: 'font: "fira code medium 20";'
}

conffiles.each_pair do |conffile, teststring|
  control "rofi-config-file-rofi-config-#{conffile}-auser-managed" do
    title 'should match desired lines'

    describe file("/home/auser/.config/rofi/#{conffile}.rasi") do
      it { should be_file }
      it { should be_owned_by 'auser' }
      it { should be_grouped_into 'auser' }
      its('mode') { should cmp '0644' }
      its('content') { should include('/* Your changes will be overwritten.') }
      its('content') { should include(teststring) }
    end
  end
end
